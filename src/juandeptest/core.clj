(ns juandeptest.core
  (:import (processing.video Movie))
  (:require [quil.core :as q]
            [clojure.core.async :as async :refer (<! <!! >! >!! put! sub pub  chan go go-loop close!)]
            [juandeptest.grid :as g]
            [quil.middleware :as m]))

(declare mov)

(defn time-stamper [t]
  (if (zero? t)
    (q/millis)
    t))

(def video-manager (chan))


(defn setup []
  (println "setup completed!")
  {:time-stamp 0
   :time 4000
   :hide-movie? false})

(defn update! [{:keys [time time-stamp] :as state}]
  (let [now (q/millis)
        screen-time (- now time-stamp)]
    
    (if (zero? time-stamp)
      (assoc state :time-stamp now)
      (condp >= screen-time
        (/ time 3) (do (put! video-manager :play)
                       (assoc state :hide-movie? false))
        time (do (put! video-manager :stop)
                 (assoc state :hide-movie? true))
        (do (put! video-manager :stop)
            (assoc state :time-stamp 0 :hide-movie? false))))))


(declare mov)

(defn movie [applet]
  (Movie. applet "resources/transit.mov"))

(defn draw [state]
  #_(q/exit)
  (q/background 100 100 100)
  (q/fill 0)
  #_(doseq [[x y] (g/grid 3 5)]
    (q/begin-shape)
    (q/stroke (rand-int 255))
    (q/rect (g/nthwidth 15)
            (g/nthheight 15)
            x y)
    (q/end-shape)
    )
  (q/text-num (:time-stamp state) 300 25)
  (q/text-num  (- (q/millis) (:time-stamp state))  300 50)
  
  (if-not (:hide-movie? state)
    (do
      (q/image mov 5 100))
    (q/rect 5 100 500 360)))

(q/defsketch juan
  :setup setup
  :middleware [m/pause-on-error m/fun-mode]
  :update update!
  :draw draw
  :movie-event (fn [movie] (.read movie))
  :size [500 500])

(def mov (movie juan))

(go-loop [command (<! video-manager)]
  (case command
    :play (.play mov)
    :stop (.stop mov)
    :else (println command))
  (recur (<! video-manager)))

