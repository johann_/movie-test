(ns juandeptest.grid
  (:require [quil.core :as q]))

(defn nthwidth
  ([n ]
     (/ (q/width) n))
  ([w n]
    (/ w n)))

(defn nthheight
  
  ([h n]
     (/ h n))
  ([n] 
     (/ (q/height) n)))

(defn width-stag
  ([w n i]
     (* (nthwidth w n) i))
  ([n i]
     (* (nthwidth n) i)))

(defn height-stag
  ([h n i]
     (* (nthheight h n) i))
  ([n i]
     (* (nthheight n) i)))

(defn img-cadence
  "creates a sequence of n x-coordinates that divide the total width of a sketch by n"
  ([w ns]
     (map (fn [idx]
            [idx  (width-stag w ns idx)]) (range ns)))
  ([ns]
     (map (fn [idx]
            [idx  (width-stag ns idx)]) (range ns))))

(defn grid
  ([xs ys]
     (for [x (range xs) y (range ys)]
       [(width-stag xs x) (height-stag ys y)])))

(defn half-grid ([xs ys]
                  (for [x (range xs) y (range ys)]
                    [(width-stag xs x) (height-stag (/ (q/height) 2)  ys y)])
                  ))
            
