(defproject juandeptest "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [cider/cider-nrepl "0.8.0-SNAPSHOT"]
                 [quil/quil "2.2.3-SNAPSHOT-JOHANN"]]
  :resource-paths ["lib/video.jar" "lib/gstreamer-java.jar" "lib/oscP5.jar" "lib/jna.jar" "lib/macosx64"])
